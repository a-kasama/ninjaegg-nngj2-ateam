﻿using UnityEngine;
using System.Collections;

public class Setting : MonoBehaviour
{
	public SettingView settingView = null;

	public void Init()
	{
		settingView.Init ();
	}

	public void OnSetting()
	{
		if (settingView.settingTimeView.gameObject.activeSelf) {
			settingView.settingTimeView.Close ();
			return;
		}
		settingView.gameObject.SetActive (!settingView.gameObject.activeSelf);
		AppBoot.eventCanvas.gameObject.SetActive (!settingView.gameObject.activeSelf);
	}

}

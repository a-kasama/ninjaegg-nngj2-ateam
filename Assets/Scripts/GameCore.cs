﻿using UnityEngine;
using System;
using System.Collections;

public class GameCore : MonoBehaviour
{

	private Timer[] timer = new Timer[4];
	private bool eventPlaying = false;

	// Use this for initialization
	void Awake () {
		eventPlaying = false;
		NewTimer (0, MorningEvent);
		NewTimer (1, LunchEvent);
		NewTimer (2, WorkAfterEvent);
		NewTimer (3, SleepEvent);
	}
	
	// Update is called once per frame
	void Update () {
		for (int i = 0; i < timer.Length; i++) {
			if (!eventPlaying) {
				timer [i].Update ();
			}
		}
	}

	private void NewTimer(int index, Timer.Delegate ev){
		DateTime now = DateTime.Now.AddMinutes (-30);
		int nowH = now.Hour;
		int nowM = now.Minute;
		int setH = PlayerPrefs.GetInt (index.ToString () + "H");
		int setM = PlayerPrefs.GetInt (index.ToString () + "M");
		if ((nowH > setH) || ((nowH == setH) && (nowM > setM))) {
			now = now.AddDays (1);
		}
		TimeSpan ts = new TimeSpan (
			              PlayerPrefs.GetInt (index.ToString () + "H"), 
			              PlayerPrefs.GetInt (index.ToString () + "M"), 0);
		timer [index] = new Timer (ts.TotalHours, ev);
	}

	private void MorningEvent(){
		eventPlaying = true;
	}

	private void LunchEvent(){
		eventPlaying = true;
	}

	private void WorkAfterEvent(){
		eventPlaying = true;
	}

	private void SleepEvent(){
		eventPlaying = true;
	}

}

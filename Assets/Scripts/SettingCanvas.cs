﻿using UnityEngine;
using System.Collections;

public class SettingCanvas : MonoBehaviour
{
	public Setting setting = null;

	void Awake()
	{
		Load ();
		setting.Init ();
		if (!PlayerPrefs.HasKey ("name")) {
			setting.OnSetting ();
		}
	}

	private void Load()
	{
		//PlayerPrefs.DeleteAll ();
		if (!PlayerPrefs.HasKey ("boot")) {
			PlayerPrefs.SetInt ("0H", 7);
			PlayerPrefs.SetInt ("0M", 0);
			PlayerPrefs.SetInt ("1H", 12);
			PlayerPrefs.SetInt ("1M", 0);
			PlayerPrefs.SetInt ("2H", 19);
			PlayerPrefs.SetInt ("2M", 0);
			PlayerPrefs.SetInt ("3H", 0);
			PlayerPrefs.SetInt ("3M", 0);
			PlayerPrefs.SetInt ("boot", 1);
			PlayerPrefs.Save ();
			Debug.Log ("Load");
		}
	}

}

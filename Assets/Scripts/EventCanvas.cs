﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class EventCanvas : MonoBehaviour {

	[SerializeField]
	private Image BackGround;

	[SerializeField]
	private Text CelifText;

	[SerializeField]
	private Text NameText;

	[SerializeField]
	private Image Background;

	[SerializeField]
	private Sprite[] BackgroundSprites;

	[SerializeField]
	private Image Character;

	[SerializeField]
	private Sprite[] CharacterSprites;

	[SerializeField]
	private Image Heart;

	public void SetCelif(string text)
	{
		CelifText.text = text;
	}

	public void SetName(string text)
	{
		NameText.text = text;
	}

	public void SetBackground(int backgroundId)
	{
		Background.sprite = BackgroundSprites [backgroundId];
	}

	public void SetCharacter(int characterId)
	{
		Character.sprite = CharacterSprites [characterId];
	}

	public void SetHeart(float value)
	{
		Heart.fillAmount = value;
	}

}

﻿using UnityEngine;
using System;
using System.Collections;

public class Timer {

	public Timer(double hourTime, Delegate timerDelegate)
	{
		limitTime = DateTime.Now;
		limitTime.AddHours (hourTime);
		TimerDelegate = timerDelegate;
	}

	private DateTime limitTime;
	public DateTime LimitTime{ get {return limitTime; }}

	public delegate void Delegate();
	private Delegate TimerDelegate;
		
	public void Update()
	{
		if (DateTime.Now > limitTime) {
			if (TimerDelegate != null) {
				TimerDelegate ();
			}
		}
	}
}

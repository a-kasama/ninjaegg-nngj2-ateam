﻿using UnityEngine;
using System;
using UnityEngine.UI;
using System.Collections;

public class SettingTimeView : MonoBehaviour
{
	public int settingIndex;
	public Text timeText;

	private Text settingText;
	private int settingHour;
	private int settingMinute;

	public void Init()
	{
		gameObject.SetActive (false);
	}

	public void Open(int index, Text text)
	{
		timeText.text = TimeString (index);

		settingIndex = index;
		settingText = text;
		settingHour = PlayerPrefs.GetInt (index.ToString() + "H");
		settingMinute = PlayerPrefs.GetInt (index.ToString() + "M");
		gameObject.SetActive (true);
	}

	public void Close()
	{
		PlayerPrefs.Save ();
		gameObject.SetActive (false);
	}

	public void OnSetTime(int addMinute)
	{
		settingMinute += addMinute;
		if (settingMinute >= 60) {
			settingHour++;
			settingMinute -= 60;
		}
		if (settingMinute < 0) {
			settingHour--;
			settingMinute += 60;
		}
		if (settingHour >= 24) {
			settingHour -= 24;
		}
		if (settingHour < 0) {
			settingHour += 24;
		}
		PlayerPrefs.SetInt (settingIndex.ToString () + "H", settingHour);
		PlayerPrefs.SetInt (settingIndex.ToString () + "M", settingMinute);

		settingText.text = TimeString (settingIndex);
		timeText.text = settingText.text;
	}

	public string TimeString(int index)
	{
		string h = PlayerPrefs.GetInt (index.ToString () + "H").ToString ().PadLeft (2, '0');
		string m = PlayerPrefs.GetInt (index.ToString () + "M").ToString ().PadLeft (2, '0');
		return h + ":" + m;

	}
}

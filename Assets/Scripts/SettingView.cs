﻿using UnityEngine;
using UnityEngine.UI;
using System;
using System.Collections;

public class SettingView : MonoBehaviour
{
	public Text nameText;
	public Text[] timeText;
	public SettingTimeView settingTimeView = null;

	private TouchScreenKeyboard keyboard = null;

	public void Init()
	{
		gameObject.SetActive (false);
		settingTimeView.Init ();

		nameText.text = PlayerPrefs.GetString ("name", "");
		for (int i = 0; i < timeText.Length; i++) {
			timeText [i].text = settingTimeView.TimeString (i);
		}
	}

	public void OnSetName()
	{
		keyboard = TouchScreenKeyboard.Open (nameText.text, TouchScreenKeyboardType.Default);
	}

	public void OnSetTimeView(int index)
	{
		settingTimeView.Open (index, timeText [index]);
	}

	void Update()
	{
		if ((keyboard != null) && (keyboard.done)) {
			nameText.text = keyboard.text;
			keyboard = null;
			PlayerPrefs.SetString ("name", nameText.text);
			PlayerPrefs.Save ();
		}
	}

}

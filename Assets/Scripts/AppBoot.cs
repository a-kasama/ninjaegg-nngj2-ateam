﻿using UnityEngine;
using System.Collections;

public class AppBoot : MonoBehaviour
{
	public GameObject root = null;

	public static EventCanvas eventCanvas = null;
	public static SettingCanvas settingCanvas= null;

	// Use this for initialization
	void Awake () {

		//腕に巻いてるイメージで肌色
		Camera.main.backgroundColor = new Color32 (252, 226, 196, 0);

		GameObject bgc = Instantiate (Resources.Load ("Prefabs/BGCanvas")) as GameObject;
		bgc.transform.SetParent (root.transform);
		bgc.transform.localPosition = Vector3.zero;
		bgc.transform.localScale = Vector3.one;
		bgc.GetComponent<Canvas> ().worldCamera = Camera.main;

		GameObject ec = Instantiate (Resources.Load ("Prefabs/EventCanvas")) as GameObject;
		ec.transform.SetParent (root.transform);
		ec.transform.localPosition = Vector3.zero;
		ec.transform.localScale = Vector3.one;
		ec.GetComponent<Canvas> ().worldCamera = Camera.main;
		eventCanvas = ec.GetComponent<EventCanvas> ();

		GameObject sc = Instantiate (Resources.Load ("Prefabs/SettingCanvas")) as GameObject;
		sc.transform.SetParent (root.transform);
		sc.transform.localPosition = Vector3.zero;
		sc.transform.localScale = Vector3.one;
		sc.GetComponent<Canvas> ().worldCamera = Camera.main;
		settingCanvas = sc.GetComponent<SettingCanvas> ();

	}
	
}

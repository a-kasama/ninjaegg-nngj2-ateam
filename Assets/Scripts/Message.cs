﻿using UnityEngine;
using System.Collections;

public class Message
{

	//女の子ver
	//仕事帰り1-1（間に合ったor30分以内にログイン）
	public static string[] work_after_1 = new string[]{
		"お疲れ様！今日もお互い大変だったね〜", 
		"そっちは体大丈夫？仕事も大変だけどあまり無理しないでね。", 
		"でも今日はあなたがデートに誘ってくれて嬉しいな。"
	};

	//女の子ver
	//仕事帰り1-2（間に合ったor30分以内にログイン）
	public static string[] work_after_2 = new string[]{
		"でも今日はあなたがデートに誘ってくれて嬉しいな", 
		"最近ゆっくり会えなかったし..."
	};

	//女の子ver
	//仕事帰り1-3（間に合ったor30分以内にログイン）
	public static string[] work_after_3 = new string[]{
		"お腹すいちゃったね。どこか食べに行こうか？", 
		"そうだ！久々にいつものお店にいってみない？"
	};

	//女の子ver
	//仕事帰り2-1（間に合わなかった&&好感度高め、とか？）
	public static string[] work_after_4 = new string[]{
		"あ、(username)やっときた！", 
		"もう、遅いから心配しちゃったー", 
		"大丈夫なら、いいの！それじゃ、いこうか？"
	};

	//女の子ver
	//仕事帰り3-1（間に合わなかった&&好感度低め、とか？）
	public static string[] work_after_5 = new string[]{
		"もう、(username)遅いよ！", 
		"こんな時間では、もうお店しまっちゃったし、", 
		"今日は疲れたから、仕方ない。もう帰ろう。。"
	};

	//女の子ver
	//朝起床時（時間通りに起きた&&平日）
	public static string[] morning_1 = new string[]{
		"おはよう、(username)", 
		"もう、まだ寝ぼけてるの？目がぼーっとしてる！", 
		"今日もお互い一日頑張ろうね。", 
		"じゃあ、また後でね！"
	};

	//女の子ver
	//朝起床時（時間通りに起きれなかった&&平日）
	public static string[] morning_2 = new string[]{
		"もーう、このおねぼうさん！", 
		"(username)、まだ寝ぼけてるの？目がぼーっとしてる！", 
		"今日もお互い一日頑張ろうね。", 
		"じゃあ、また後でね！"
	};

	//女の子ver
	//朝起床時（時間通りに起きた&&休日）
	public static string[] morning_3 = new string[]{
		"おはよう。(username)", 
		"昨日はよく寝られた？顔がまだ眠そうだよ〜", 
		"今日の約束、大丈夫？", 
		"じゃあ、また後でね！"
	};

	//女の子ver
	//朝起床時（時間通りに起きれなかった&&休日）
	public static string[] morning_4 = new string[]{
		"おはよう。(username)", 
		"もう、、まだおきてないの？", 
		"今日の約束、大丈夫？", 
		"じゃあ、また後でね！"
	};

	//女の子ver
	//昼（間に合った）
	public static string[] lunch_1 = new string[]{
		"あ、もうこんな時間！", 
		"(username)、一緒にご飯たべよ！"
	};

	//女の子ver
	//昼（間に合わなかった）
	public static string[] lunch_2 = new string[]{
		"あ、(username)", 
		"もう、遅いから先に食べちゃった。。", 
		"ごめんね。私午後から会議あるから、いくね？"
	};

	//女の子ver
	//寝る前
	public static string[] sleep_1 = new string[]{
		"あ、(username)？", 
		"よかった。まだおきてた！", 
		"今日はありがと！", 
		"ゆっくりやすんでね。おやすみなさいー"
	};

	//女の子ver
	//寝る前(おそくなった)
	public static string[] sleep_2 = new string[]{
		"(username)", 
		"もう、寝ちゃったのかと思ってた。。", 
		"今日はもう寝ようか", 
		"ゆっくりやすんでね。おやすみなさいー"
	};











}